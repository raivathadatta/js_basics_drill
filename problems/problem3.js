
//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.
function displayStudentsGivenCountryName(data,country){
 try{
    if(Array.isArray(data)){
        let students=[]
        let index=0
        while(index<data.length){
            if(data[index].isStudent && data[index].country.toLowerCase()==country.toLowerCase()){
                students.push(data[index].name)
            }
            index++
        }
        console.log( students + " "+"Lives in "+country);
    }else{
        throw 'input miss match'
    }
 }catch(error){
    console.log(error)
 }
}

export default displayStudentsGivenCountryName