
//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.
function displayHobbies(data, age) {

    try {
        if (Array.isArray(data)) {
            let hobbies = []
            for (let index = 0; index < data.length; index++) {
                if (data[index].age == age) {
                    for (let index1 = 0; index1 < data[index].hobbies.length; index1++) {
                        hobbies.push(data[index].hobbies[index1])
                    }
                }
            }
            console.log(hobbies)
        } else {
            throw "expected array "
        }
    } catch (error) {
        console.log(error)
    }
}

export default displayHobbies

