//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.

function getEmails(data) {
    try {
        if (Array.isArray(data)) {
            let emails = [];
            for (let index = 0; index < data.length; index++) {
                emails.push(data[index].email)
            }
            return emails
        } else {
            throw 'input miss match'
        }
    } catch (error) {
        console.log(error)
    }
}

export default getEmails
