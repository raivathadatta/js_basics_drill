

//    Implement a loop to access and log the city and country of each individual in the dataset.
function displayCityAndCountry(data) {
   try {
      if (!Array.isArray(data)) {
         throw 'input miss match'
      }
      for (let index = 0; index < data.length; index++) {
         console.log(data[index].name + " is from" + data[index].city + " -" + data[index].country)
      }
   } catch (error) {
      console.log(error)
   }
}
export default displayCityAndCountry
