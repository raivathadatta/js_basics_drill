
//    Implement a loop to access and print the ages of all individuals in the dataset.

function displayAges(data) {

    try {
        if (!Array.isArray(data)) {
            throw 'input miss match'
        }

        for (let index = 0; index < data.length; index++) {

            console.log(data[index].age)
        }

    } catch (error) {
        console.log(error)
    }
}
export default displayAges

