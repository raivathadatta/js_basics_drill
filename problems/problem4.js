//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

function displayNameAndCityNameAtIndex(data, index) {
    try {
        if (Array.isArray(data)) {
            let obj = {
                name: data[index].name,
                city: data[index].city
            }
            console.log(obj)
        } else {
            throw 'input miss match'
        }
    } catch (error) {
        console.log(error)
    }
}

export default displayNameAndCityNameAtIndex