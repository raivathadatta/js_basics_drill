import arrayOfObjects from "../index.js";
import getEmails from "../problems/problem1.js";
import displayHobbies from "../problems/problem2.js";
import displayStudentsGivenCountryName from "../problems/problem3.js";
import displayNameAndCityNameAtIndex from "../problems/problem4.js";
import toDisplayFirstHobbyOfInndividual from "../problems/problem5.js"

import displayAges from "../problems/problem6.js";
import displayGetNameAndEmail from "../problems/problem7.js";

import displayCityAndCountry from "../problems/problem8.js";

const emails = getEmails(arrayOfObjects)
console.log("///////////all emails/////////")
console.log(emails)
console.log("")
console.log("/////////hobbies of age 33//////////")

displayHobbies(arrayOfObjects,33)
console.log("")
console.log("//////////getStudentsGivenCountryName/////////")

displayStudentsGivenCountryName(arrayOfObjects,'Australia')

console.log("")
console.log("//////////getNameAndCityNameAtIndex/////////")

displayNameAndCityNameAtIndex(arrayOfObjects,3)



console.log("")
console.log("//////////toDisplayFirstHobbyOfInndividual/////////")

toDisplayFirstHobbyOfInndividual(arrayOfObjects)

console.log("")
console.log("//////////getAges/////////")


displayAges(arrayOfObjects)
console.log("")
console.log("//////////toGetNameAndEmail/////////")


displayGetNameAndEmail(arrayOfObjects)
console.log("")
console.log("//////////displayCityAndCountry/////////")

displayCityAndCountry(arrayOfObjects)